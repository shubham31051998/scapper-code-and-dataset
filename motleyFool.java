import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.w3c.dom.css.Counter;


public class motleyFool {
	public motleyFool(){
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "chromedriver");                          //Set driver to appropriate location
        driver = new ChromeDriver();																														
        int counter = 1;
        while(counter <= 20){
        	driver.get("https://www.fool.com/earnings-call-transcripts/?page=" + counter);      //Loop through the site to get url 
        	List<WebElement> urls;
	        urls = driver.findElements(By.xpath("//div[@class='text']/h4/a"));					//Find link of url	in D.O.M.
	        for (WebElement url : urls){
	        	System.out.println(url.getAttribute("href"));							
	        	
	        }
	        counter++;
        }
        driver.close();
	}
}
