import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class motleyFoll_ExtractData {
	public motleyFoll_ExtractData(){
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "chromedriver");  //Set location of chrome driver
        driver = new ChromeDriver();
        Scanner scanner = null;
		try {
			scanner = new Scanner(new File("Motleyfool.txt"));          //Open file which contains url of data to be scrapped
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String file_name;
		FileWriter fw = null;
		int counter = 1;
		while (scanner.hasNextLine()) {  
			   String line = scanner.nextLine();
			   int idx = line.lastIndexOf('/') + 1; 
			   counter++;
			   if(counter <= 9){
				   continue;
			   }
			   if(idx == 0){
				   continue;
			    }
			    file_name = line.substring(idx) + ".txt";              //Get the name of company
			    try {
					fw =new FileWriter("Motley/" + file_name);         //Create file of same name as that of company 
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
			    driver.get(line);
			    List<WebElement> span;
			    span = driver.findElements(By.xpath("//span[@class='article-content']"));  //Select appropriate span which contains data
			    for (WebElement text : span){
			    	try {
						fw.write(text.getText());
						fw.write("\n");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
			    		
			    }
			    System.out.println("Success "+ counter);
			    try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
		}
	}
	

}
