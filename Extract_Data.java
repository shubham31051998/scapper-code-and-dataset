import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

//Entire operation is performed in constructor
public class Extract_Data {
	public Extract_Data(){
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File("Nasdaq2.txt"));               //Open file which contains url of data to be scrapped
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		int count = 0;
		String file_name;
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "chromedriver");    //Set location of chrome driver
		driver = new ChromeDriver();
		FileWriter fw = null;
		while (scanner.hasNextLine()) {  
		   String line = scanner.nextLine();
		   System.out.println(line);
		   int idx = line.indexOf("Title") + 6;				              //Get the name of company
		   if(idx == 5){
			   continue;
		    }
		    file_name = line.substring(idx) + ".txt";                     //Create file of same name as that of company
		    driver.get(line);        
		    try {
				fw =new FileWriter(file_name);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        List<WebElement> paragraphs;
	        paragraphs = driver.findElements(By.xpath("//p"));            //Read all paragraph which contains required data
	        for (WebElement text : paragraphs)  
	        { 
	        	try {
					fw.write(text.getText());                             //Write data to file       
					fw.write("\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	        }
	        System.out.println("Success");
	        try {
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}    
		} 
	}
}
