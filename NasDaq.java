import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.w3c.dom.css.Counter;

public class NasDaq {
	public NasDaq(){
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "chromedriver"); 
        driver = new ChromeDriver();
        driver.get("https://www.nasdaq.com/symbol/dow/call-transcripts");         //Go to base url   
        int Counter = 1;
        while(Counter <= 10){													  //Loop through entire list to different page to get link  	
	        List<WebElement> anchor;
	        anchor = driver.findElements(By.xpath("//table[@id='quotes_content_left_CalltranscriptsId_CallTranscripts']/tbody/tr[" + Counter + "]/td/span/a"));
	        for (WebElement text : anchor)  
	        { 
	        	System.out.println(text.getAttribute("href"));                   //Print the links scrapped
	        }
	        Counter++;
       }
       driver.close(); 
	}
}
